<?php
// A simple script to securely return otters

// Use param store to fetch secrets
require '../php/aws/aws-autoloader.php';
include '../php/orm.php';

use Aws\Ssm\SsmClient;
$client = new SsmClient([
    'version' => 'latest',
    'region' => 'us-east-1',
]);

$db_name = $client->getParameter([
	'Name' => 'otters-database_name',
])['Parameter']['Value'];

$db_username = $client->getParameter([
	'Name' => 'otters-database_username',
	'WithDecryption' => true
])['Parameter']['Value'];

$db_password = $client->getParameter([
	'Name' => 'otters-database_password',
	'WithDecryption' => true
])['Parameter']['Value'];

$db_address = $client->getParameter([
	'Name' => 'otters-database_address',
])['Parameter']['Value'];

$conn = new mysqli($db_address, $db_username, $db_password);

SimpleOrm::useConnection($conn, $db_name);

class Otters extends SimpleOrm { }

$num_otters = count(Otters::all());
$otter_id = random_int (1, $num_otters);
$otter = Otters::retrieveByPK($otter_id);

$instance_id = file_get_contents("http://169.254.169.254/latest/meta-data/instance-id");

?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Random Otter Generator</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
	<center>
		<img src="<?= $otter->otter_gif ?>" />
		<br/>
		<pre>Served from : <?= $instance_id ?>
	</center>
</body>
</html>