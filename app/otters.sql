create table otters (
	id INT,
	otter_gif VARCHAR(255)
);
insert into otters (id, otter_gif) values (1, 'https://assets.rbl.ms/10331165/980x.gif');
insert into otters (id, otter_gif) values (2, 'https://assets.rbl.ms/10331166/980x.gif');
insert into otters (id, otter_gif) values (3, 'https://assets.rbl.ms/10331169/980x.gif');
insert into otters (id, otter_gif) values (4, 'https://assets.rbl.ms/10331170/980x.gif');
insert into otters (id, otter_gif) values (5, 'https://media1.tenor.com/images/e84bff33cbd2ff7a40cd9bbf3a4f0024/tenor.gif?itemid=11113562');
insert into otters (id, otter_gif) values (6, 'https://media1.giphy.com/media/26gssIytJvy1b1THO/giphy.gif');
insert into otters (id, otter_gif) values (7, 'https://media1.tenor.com/images/bc8336a449679407c58a98408c1a1ece/tenor.gif?itemid=8905253');
insert into otters (id, otter_gif) values (8, 'https://i.imgur.com/YwQeHUG.gif');
insert into otters (id, otter_gif) values (9, 'https://thumbs.gfycat.com/ImportantAntiqueAustralianshelduck-size_restricted.gif');
insert into otters (id, otter_gif) values (10, 'https://gifer.com/i/1y0i.gif');