# Simple Two Tier Web App

This is a simple two tier web application which shows cute otter gifs.

## Rough Outline

https://cloudcraft.co/view/9b436c7f-7fdf-4d72-8ff5-75195b9c9642?key=IfV7MRKbnwaLU5JJk4I04g&embed=true

## Project Structure

```shell
|
| .
| -> app (our super basic php app to show otters)
| -> terraform (setting up aws to run our application)
|
```

## Prerequirements

- AWS Account with credentials to access VPC, EC2 inc SSM, RDS, SSM, ACM, KMS and Route53
- Latest Terraform installed

## Getting started with the terraform

``
cd terraform
terraform init
terraform plan --out plan.out
terraform apply plan.out
```

## Terraform Structure

- acm.tf : setup an SSL certificate and validates it via Route53
- data.tf : preforms our lookups for existing resources such as the domain and ami
- ec2_asg.tf : creation of the autoscaling group and launch configuration
- ec2_lb.tf : creates a classic load balancer
- ec2_param_store.tf : a place to keep some secrets
- ec2_security_groups.tf : setups up four security groups (http ingress to elb, http ingress from elb, ssh from your ip, allows mysql from web)
- ec2_userdata.tf : templates a hacky bash script for bootstrapping out servers
- ec2.tf : setups up a ssh keypair
- iam_kms.tf : setup a KMS key for use with param store
- iam.tf : creates an instance role to access param store and kms
- my_ip.tf : uses http provider to get my ip for use in the ssh security group
- random.tf : use random providers for db setup
- rds.tf : create a mysql database
- remote_state.tf : securely store our state in S3
- route53.tf : adds a entry for the ELB into our primary zone
- variables.tf : allow you to customize what gets deployed some what
- vpc_subnets.tf : creates our vpc subnets
- vpc.tf : vpc creation

## What's next 

- Make it more modular, split things out into own terraform modules
- Use Ansible instead of bash scriping to bootstrap
- Use packer to bake an AMI and speed up time to live
- The AMI should have some hardening too
- More otters?