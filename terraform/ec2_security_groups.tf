#
# ELB Security Group
resource "aws_security_group" "elb" {
  name        = "${var.app_name}-elb"
  description = "Allow all inbound HTTP traffic"
  vpc_id      = "${aws_vpc.main.id}"

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "TCP"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "TCP"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }
}


resource "aws_security_group" "web" {
  name        = "${var.app_name}-web"
  description = "Allow traffic from the ELB"
  vpc_id      = "${aws_vpc.main.id}"

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "TCP"
    security_groups = ["${aws_security_group.elb.id}"]
  }

  ingress {
    from_port   = 8443
    to_port     = 8443
    protocol    = "TCP"
    security_groups = ["${aws_security_group.elb.id}"]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "ssh" {
  name        = "${var.app_name}-ssh"
  description = "Allow ssh traffic from my IP"
  vpc_id      = "${aws_vpc.main.id}"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "TCP"
    cidr_blocks = ["${trimspace(data.http.my_ip.body)}/32"]
  }
}

resource "aws_security_group" "mysql" {
  name        = "${var.app_name}-mysql"
  description = "Allow mysql traffic from web"
  vpc_id      = "${aws_vpc.main.id}"

  ingress {
    from_port   = 3306
    to_port     = 3306
    protocol    = "TCP"
    security_groups = ["${aws_security_group.web.id}"]
  }
}