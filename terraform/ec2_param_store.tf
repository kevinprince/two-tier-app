resource "aws_ssm_parameter" "database_username" {
  name  = "${var.app_name}-database_username"
  type  = "SecureString"
  value = "${aws_db_instance.default.username}"
  key_id = "${aws_kms_key.default.arn}"
}

resource "aws_ssm_parameter" "database_password" {
  name  = "${var.app_name}-database_password"
  type  = "SecureString"
  value = "${random_string.db_password.result}"
  key_id = "${aws_kms_key.default.arn}"
}

resource "aws_ssm_parameter" "database_address" {
  name  = "${var.app_name}-database_address"
  type  = "String"
  value = "${aws_db_instance.default.address}"
}

resource "aws_ssm_parameter" "database_name" {
  name  = "${var.app_name}-database_name"
  type  = "String"
  value = "${random_id.db_name.hex}"
}