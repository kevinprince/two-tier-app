#
# IAM : KMS Encryption Key
resource "aws_kms_key" "default" {
  description             = "encrypted-${var.app_name}"
  deletion_window_in_days = 10
}

resource "aws_kms_alias" "default" {
  name          = "alias/${var.app_name}"
  target_key_id = "${aws_kms_key.default.key_id}"
}