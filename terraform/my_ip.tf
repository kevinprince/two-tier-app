data "http" "my_ip" {
  url = "https://ifconfig.co/ip"

  # Optional request headers
  request_headers {
    "Accept" = "application/json"
  }
}