resource "aws_route53_record" "web" {
  zone_id = "${data.aws_route53_zone.selected.zone_id}"
  name    = "${var.app_name}.${var.domain_name}"
  type    = "A"

  alias {
    name                   = "${aws_elb.web.dns_name}"
    zone_id                = "${aws_elb.web.zone_id}"
    evaluate_target_health = true
  }
}