#
# Whos calling please
data "aws_caller_identity" "current" {}

#
# Where are we?
data "aws_region" "current" {}

#
# Available Availability Zones
data "aws_availability_zones" "available" {}

#
# Latest Ubuntu AMI
data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-bionic-18.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}

#
# User Data for instance
data "template_file" "userdata" {
  template = "${file("ec2_userdata.tpl")}"

  vars {
    app_name = "${var.app_name}"
  }
}

#
# Route 53 Domain
data "aws_route53_zone" "selected" {
  name = "${var.domain_name}."
}