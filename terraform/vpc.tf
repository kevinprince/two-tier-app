#
# Simple VPC
resource "aws_vpc" "main" {
  cidr_block = "192.168.0.0/16"

  tags {
    Name = "${var.app_name}"
  }
}

#
# Internet Gateway
resource "aws_internet_gateway" "gw" {
  vpc_id = "${aws_vpc.main.id}"
}

#
# Add internet routing
resource "aws_route" "main" {
  route_table_id         = "${aws_vpc.main.default_route_table_id}"
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = "${aws_internet_gateway.gw.id}"
  depends_on             = ["aws_vpc.main"]
}
