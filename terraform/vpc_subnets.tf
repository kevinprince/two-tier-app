resource "aws_subnet" "main" {
  count             = "${var.subnet_count}"
  vpc_id            = "${aws_vpc.main.id}"
  cidr_block        = "${cidrsubnet("${aws_vpc.main.cidr_block}", 8, count.index)}"
  availability_zone = "${data.aws_availability_zones.available.names[count.index]}"
  map_public_ip_on_launch = true
}
