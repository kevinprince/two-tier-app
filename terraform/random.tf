resource "random_id" "db_name" {
  byte_length = "2"
}

resource "random_id" "db_username" {
    byte_length = "4"
}

resource "random_string" "db_password" {
  length = 32
  special = false
}