variable "app_name" {
  default = "otters"
}

variable "domain_name" {
  default = "myio.pw"
}
variable "subnet_count" {
  default = 2
}

variable "ssh_username" {
  default = "kevinprince-cros"
}

variable "ssh_pubkey" {
  default = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC4OIbRBFH54i+o2CyRigiZ9vdNJuuAYcNEdJER779xUuq68u1pC1FiEDM9dn/NUN0ABuVYBMFd7D5XR3HOTh1TkNxZnWaPNeIRgFFovxuTTGFntFe7a6tfgSIPOqcLr9j6w7rbGuulDa2Ft0hXfJTtyGdu/LpoExSQMXJscKJ/K4xQQ2JwZ9+lSZQP3RDVWrDH5Rtmk8PZIC5/RtY7bj+X642ftiAQoYWDTXLTN+Lnad7aYfof/0xYMAvM6/GyzEW1+O3FLiMzzmxYsAeoBAAGBxC4NgV56781osxJYjhAXFeDpbNqZP2Z0PuByNSWI4+p4Dpgc0Zm4J0V9ETuxpZz nextgengames@penguin"
}

