#
# IAM Instance Profile
resource "aws_iam_instance_profile" "web" {
  name = "${var.app_name}-web"
  role = "${aws_iam_role.web.name}"
}

resource "aws_iam_role" "web" {
  name = "${var.app_name}-web"
  path = "/"

  assume_role_policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": "sts:AssumeRole",
            "Principal": {
               "Service": "ec2.amazonaws.com"
            },
            "Effect": "Allow",
            "Sid": ""
        }
    ]
}
EOF
}

resource "aws_iam_policy" "web" {
    name        = "${var.app_name}-instance-policy"
    description = "${var.app_name} allow param store and kms access"
    policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
  {
    "Sid": "0",
    "Effect": "Allow",
    "Action": [
      "kms:Decrypt",
      "ssm:GetParameter"
    ],
    "Resource": [
      "arn:aws:ssm:${data.aws_region.current.name}:${data.aws_caller_identity.current.account_id}:parameter/${var.app_name}*",
      "arn:aws:kms:${data.aws_region.current.name}:${data.aws_caller_identity.current.account_id}:key/${aws_kms_key.default.key_id}"
    ]
  }
  ]
}

EOF
}

resource "aws_iam_role_policy_attachment" "web-attach" {
    role       = "${aws_iam_role.web.name}"
    policy_arn = "${aws_iam_policy.web.arn}"
}