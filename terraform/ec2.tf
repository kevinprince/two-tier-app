#
# Keypair to allow SSH in.
resource "aws_key_pair" "admin" {
  key_name   = "${var.ssh_username}"
  public_key = "${var.ssh_pubkey}"
}
