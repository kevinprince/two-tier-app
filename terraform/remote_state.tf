terraform {
  backend "s3" {
    bucket = "myio-terraform-state"
    key    = "otters"
    region = "us-east-1"
    encrypt = true 
  }
}
