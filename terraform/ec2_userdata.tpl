#!/bin/bash
sudo su
apt-get update
apt-get upgrade -y
apt-get install -y nginx php-fpm php-mysql mysql-client python unzip jq

#Delete the existing nginx default config
curl https://gitlab.com/kevinprince/two-tier-app/raw/master/app/vhost.conf > /etc/nginx/sites-available/default

#Install AWS CLI
curl https://bootstrap.pypa.io/get-pip.py | python
pip install awscli
export AWS_DEFAULT_REGION=$(curl --silent http://169.254.169.254/latest/dynamic/instance-identity/document | jq -r .region)

#Grab DB info for restoring the DB
export DB_ADDRESS=$(aws ssm get-parameter --name "${app_name}-database_address" | jq --raw-output .Parameter.Value)
export DB_NAME=$(aws ssm get-parameter --name "${app_name}-database_name" | jq --raw-output .Parameter.Value)
export DB_USERNAME=$(aws ssm get-parameter --name "${app_name}-database_username" --with-decryption | jq --raw-output .Parameter.Value)
export MYSQL_PWD=$(aws ssm get-parameter --name "${app_name}-database_password" --with-decryption | jq --raw-output .Parameter.Value)


#Check if otters db is available and restore if needed
if ! mysql -h $DB_ADDRESS -u $DB_USERNAME -e 'use '$DB_NAME; then
    curl https://gitlab.com/kevinprince/two-tier-app/raw/master/app/${app_name}.sql > dump.sql
    mysql -h $DB_ADDRESS -u $DB_USERNAME -e 'create database '$DB_NAME;
    mysql -h $DB_ADDRESS -u $DB_USERNAME $DB_NAME < dump.sql
fi

#Install our otters app
mkdir /var/www/php
mkdir /var/www/php/aws
wget http://docs.aws.amazon.com/aws-sdk-php/v3/download/aws.zip
unzip aws.zip -d /var/www/php/aws

curl https://raw.githubusercontent.com/noetix/Simple-ORM/master/SimpleOrm.class.php > /var/www/php/orm.php

rm /var/www/html/index*
curl https://gitlab.com/kevinprince/two-tier-app/raw/master/app/${app_name}.php > /var/www/html/index.php

/etc/init.d/nginx restart