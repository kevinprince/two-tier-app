resource "aws_db_subnet_group" "default" {
  name       = "${var.app_name}"
  subnet_ids = ["${aws_subnet.main.*.id}"]

  tags {
    Name = "${var.app_name} db subnet group"
  }
}

resource "aws_db_instance" "default" {
  allocated_storage    = 10
  storage_type         = "gp2"
  engine               = "mysql"
  engine_version       = "5.7"
  instance_class       = "db.t2.micro"
  identifier_prefix    = "${var.app_name}"
  username             = "${var.app_name}${random_id.db_username.hex}"
  password             = "${random_string.db_password.result}"
  parameter_group_name = "default.mysql5.7"
  db_subnet_group_name = "${aws_db_subnet_group.default.id}"
  vpc_security_group_ids = ["${aws_security_group.mysql.id}"]
  skip_final_snapshot    = true
}