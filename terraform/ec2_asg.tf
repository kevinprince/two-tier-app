resource "aws_autoscaling_group" "web" {
  name = "${var.app_name}-web"

  vpc_zone_identifier = ["${aws_subnet.main.*.id}"]

  desired_capacity     = 2
  max_size             = 2
  min_size             = 1
  launch_configuration = "${aws_launch_configuration.web.id}"
  load_balancers = ["${aws_elb.web.name}"]
}

resource "aws_launch_configuration" "web" {
  image_id      = "${data.aws_ami.ubuntu.id}"
  instance_type = "t2.micro"
  spot_price    = "0.01"

  security_groups = [
    "${aws_security_group.web.id}",
    "${aws_security_group.ssh.id}"
  ]

  user_data = "${data.template_file.userdata.rendered}"
  key_name  = "${aws_key_pair.admin.key_name}"
  iam_instance_profile = "${aws_iam_instance_profile.web.name}"

  lifecycle {
    create_before_destroy = true
  }
}
